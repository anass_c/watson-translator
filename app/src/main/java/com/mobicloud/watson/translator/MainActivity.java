package com.mobicloud.watson.translator;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;

public class MainActivity extends AppCompatActivity {

    private String originalLang;
    private String transLang;
    private Spinner spinner, spinner1;

    private AppCompatEditText mOriginalText;
    private AppCompatTextView mTranslatedText;

    private FloatingActionButton pasteText, deleteText, copyText, convertText;

    private LanguageTranslator service;

    private ProgressBar mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mOriginalText = findViewById(R.id.original_text_et);
        mTranslatedText = findViewById(R.id.translated_text_tv);


        mProgressBar = findViewById(R.id.progress_circular);
        mProgressBar.setVisibility(ProgressBar.INVISIBLE);

        convertText = findViewById(R.id.translate_text__btn);
        convertText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userTextInput = getOriginalText();

                if (TextUtils.isEmpty(userTextInput)) {
                    Toast.makeText(MainActivity.this, "Content to translate cannot be empty ...", Toast.LENGTH_SHORT).show();
                } else {
                    new WatsonTranslator().execute(userTextInput, originalLang, transLang);
                }
            }
        });

        deleteText = findViewById(R.id.cancel_btn);
        deleteText.setOnClickListener(v -> mOriginalText.setText(""));

        copyText = findViewById(R.id.copy_translation_btn);
        copyText.setOnClickListener(v -> {
            if (mTranslatedText.getText() != "") {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("copy", mTranslatedText.getText());
                clipboard.setPrimaryClip(clip);
                show("Translated Text have been copied to clip board");
            } else {
                show("You haven't translated any thing yet");
            }
        });

        pasteText = findViewById(R.id.paste_btn);

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

        mOriginalText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!(clipboard.hasPrimaryClip())) {

                    pasteText.setEnabled(false);

                } else if (!(clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {

                    // This disables the paste menu item, since the clipboard has data but it is not plain text
                    pasteText.setEnabled(false);
                } else {

                    // This enables the paste menu item, since the clipboard contains plain text.
                    pasteText.setEnabled(true);
                }
            }
        });

        pasteText.setOnClickListener(v -> {
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

            // Gets the clipboard as text.
            String pasteData = (String) item.getText();

            // If the string contains data, then the paste operation is done
            if (pasteData != null) {
                mOriginalText.setText(pasteData);

                // The clipboard does not contain text. If it contains a URI, attempts to get data from it
            }

        });

    }


    class WatsonTranslator extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(ProgressBar.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            Authenticator authenticator = new IamAuthenticator("unrFJNkDNMyIa3JvdgLHFJDlj9olzhr2ZWeo7J1xQWTs");
            service = new LanguageTranslator("2019-12-26", authenticator);
            service.setServiceUrl("https://api.eu-gb.language-translator.watson.cloud.ibm.com/instances/96da4778-36b9-4d7e-ac15-cfba848afaa6");

            TranslateOptions translateOptions = new TranslateOptions.Builder()
                    .addText(strings[0])
                    .source(strings[1])
                    .target(strings[2])
                    .build();
            TranslationResult translationResult = service.translate(translateOptions).execute().getResult();
            return extractDataFromJsonResponse(translationResult.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            mTranslatedText.setText(s);
        }

        private String extractDataFromJsonResponse(String jsonObj) {

            String res = "";

            try {
                JSONObject jsonObject = new JSONObject(jsonObj);
                JSONArray jsonArray = jsonObject.getJSONArray("translations");
                res = jsonArray.getJSONObject(0).getString("translation");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }
    }

    private String getOriginalText() {
        return Objects.requireNonNull(mOriginalText.getText()).toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.text_translate_menu, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        populateFirstSpinner(item);

        MenuItem item2 = menu.findItem(R.id.spinner1);
        populateSecondSpinner(item2);

        return super.onCreateOptionsMenu(menu);
    }

    private void populateFirstSpinner(MenuItem item) {
        spinner = (Spinner) item.getActionView();
        ArrayAdapter<CharSequence> languageAdapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_list_item_array, android.R.layout.simple_spinner_item);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(languageAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                originalLang = item;
                if (spinner1.getSelectedItemPosition() == position)
                    spinner1.setSelection((position + 1) % 3);
//                show(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void populateSecondSpinner(MenuItem item) {
        spinner1 = (Spinner) item.getActionView();
        ArrayAdapter<CharSequence> languageAdapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_list_item_array, android.R.layout.simple_spinner_item);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(languageAdapter);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                transLang = item;
                if (spinner.getSelectedItemPosition() == position)
                    spinner.setSelection((position + 1) % 3);
//                show(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void show(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}
